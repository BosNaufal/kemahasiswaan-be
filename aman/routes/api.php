<?php

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => ['cors']], function() {
    Route::resource('file', 'FileController', ['only' => ['store', 'destroy', 'show']]);

    Route::post('graph', 'GraphQLController@index');
    Route::get('graph', 'GraphQLController@index');

    Route::post('login', 'UserController@login');
    Route::delete('logout', 'UserController@logout');
    Route::get('auth', 'UserController@authCheck');
    
    Route::apiResource('users', 'UserController');
    Route::apiResource('beasiswa', 'BeasiswaController');
    Route::apiResource('pendaftar', 'PendaftarController');
    Route::post('pendaftar/selesai/{beasiswaId}', 'PendaftarController@bulkUpdateStatus');
    Route::post('umumkan', 'PendaftarController@umumkan');

    Route::post('analytic', 'BeasiswaController@analytic');
    Route::get('analytic/scoreboard', 'BeasiswaController@scoreboard');
    // Route::get('umumkan', function() {
    //     $mail = new PHPMailer;

    //     // Konfigurasi SMTP
    //     $mail->isSMTP();
    //     $mail->Host = 'smtp.gmail.com';
    //     $mail->SMTPAuth = true;
    //     $mail->Username = 'admiinPISI@gmail.com';
    //     $mail->Password = 'bunitaclub';
    //     $mail->SMTPSecure = 'tls';
    //     $mail->Port = 587;

    //     $mail->setFrom('admiinPISI@gmail.com', 'Admin Beasiswa UINSA');
    //     $mail->addReplyTo('admiinPISI@gmail.com', 'Admin Beasiswa UINSA');

    //     // Menambahkan penerima
    //     $mail->addAddress('ahmaadfaaruq@gmail.com');

    //     // Menambahkan cc atau bcc 
    //     $mail->addCC('lailatulm185@gmail.com');
    //     // $mail->addBCC('bcc@contoh.com');

    //     // Subjek email
    //     $mail->Subject = 'Pengumuman Beasiswa';

    //     // Mengatur format email ke HTML
    //     $mail->isHTML(true);

    //     // Konten/isi email
    //     $mailContent = "<h1>Mengirim Email HTML menggunakan SMTP di PHP</h1>
    //         <p>Ini adalah email percobaan yang dikirim menggunakan email server SMTP dengan PHPMailer.</p>";
    //     $mail->Body = $mailContent;

    //     // Kirim email
    //     if (!$mail->send()){
    //         echo 'Pesan tidak dapat dikirim.';
    //         echo 'Mailer Error: ' . $mail->ErrorInfo;
    //     } else{
    //         echo 'Pesan telah terkirim';
    //     }
    // });
    
    Route::resource('location', 'LocationController', ['only' => ['index']]);
    Route::apiResource('categories', 'CategoriesController');
    Route::apiResource('ads', 'AdsController');
});
