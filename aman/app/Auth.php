<?php

namespace App;
use App\BaseModal;

class Auth extends BaseModel
{
    protected $table = "auth";
    protected $primaryKey = "token";

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
