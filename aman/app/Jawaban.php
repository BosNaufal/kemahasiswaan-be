<?php

namespace App;

use App\BaseModal;

class Jawaban extends BaseModel
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
  */
  protected $fillable = [
    "pendaftar_id",
    'question_id',
    'question',
    'required',
    'type',
    'value',
    'answers',
  ];
    
  public function pendaftar()
  {
      return $this->belongsTo('App\Pendaftar');
  }
}
