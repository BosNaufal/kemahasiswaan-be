<?php

namespace App;

use App\BaseModal;

class Beasiswa extends BaseModel
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
  */
  protected $fillable = [
    'judul',
    'published_date',
    'email_body',
    'konten',
    'form',
  ];
    
  public function pendaftars()
  {
      return $this->hasMany('App\Pendaftar');
  }
}
