<?php

namespace App;

use App\BaseModal;

class User extends BaseModel
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
  */
  protected $fillable = [
    "email",
    "nama",
    'password',
  ];
    
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
  */
  protected $hidden = [
    'password',
  ];

  public function auth()
  {
      return $this->hasOne('App\Auth');
  }
}
