<?php

namespace App;

use App\BaseModal;

class Pendaftar extends BaseModel
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
  */
  protected $fillable = [
    "beasiswa_id",
    "nama",
    "email",
    "gender",
    "status",
    "last_status",
  ];
    
  public function beasiswa()
  {
      return $this->belongsTo('App\Beasiswa');
  }

  public function jawabans()
  {
      return $this->hasMany('App\Jawaban');
  }
}
