<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Auth;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', [
            'only' => [
                "destroy",
                "update",
                "logout",
            ]
        ]);
    }

    protected function makeErrors($messages, $code = 400) {
        return response()->json(["error" => true, "messages" => $messages], $code);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->query();

        $perPage = !empty($params['per_page']) 
            ? $params['per_page']
            : 10;
        
        $currentPage = !empty($params['page']) 
            ? $params['page']
            : 1;

        $usersQuery = User::orderBy("created_at", "desc");
        
        $usersCount = $usersQuery->count();
        $users = $usersQuery
            ->limit($perPage)
            ->offset($currentPage - 1)
            ->get();

        if ($users) {
            $meta = [
                "per_page" => $perPage,
                "page" => $currentPage,
                "total_data" => $usersCount
            ];
            return response()->json([
                "results" => $users,
                "meta" => $meta,
            ]);
        } else {
            return $this->makeErrors(["No user found"]);
        }
    }

    public function authCheck(Request $request)
    {
        $params = $request->query();
        if (empty($params["token"])) {
            return $this->makeErrors(["Token not supplied"]);
        }

        $user = User::whereHas('auth', function($query) use ($params) {
            $query->where('token', $params["token"]);
        })->first();
        
        if ($user) {
            $user->token = $params["token"];
            unset($user->auth);
            return response()->json($user);
        } else {
            return $this->makeErrors(["Token not found"]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function login(Request $request)
    {
        $allInput = $request->all();

        $validator = Validator::make($allInput, [
            "email" => 'required',
            "password" => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->makeErrors($errors);
        }

        $user = User::where("email", $allInput["email"])
            ->first();
        
        if ($user) {
            $validPassword = Hash::check($allInput["password"], $user->password);
            if ($validPassword) {
                $auth = Auth::where("user_id", $user->id)->first();
                if (!$auth) {
                    $auth = new Auth();
                    $auth->user_id = $user->id;
                    $auth->save();
                }
                $token = $auth->token;
                $user->token = $token;
                return response()->json($user);
            } else {
                return $this->makeErrors(["Password is invalid"]);
            }
        } else {
            return $this->makeErrors(["User not found"]);
        }
    }


    public function logout(Request $request)
    {
        $token = $request->auth->token;
        $auth = Auth::find($token);
        if ($auth) {
            $deleted = $auth->delete();
            if ($deleted) {
                return response()->json($deleted);
            } else {
                return $this->makeErrors(["Database error"], 500);
            }
        } else {
            return $this->makeErrors(["Token not found"]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //    
    }

    private function validateUser($allInput, $isCreate = false) {
        $validator = Validator::make($allInput, [
            "email" => 'required|unique:users',
            "nama" => 'required',
            "password" => $isCreate ? 'required' : '',
        ]);
        return $validator;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allInput = $request->all();
        $validator = $this->validateUser($allInput, true);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->makeErrors($errors);
        }

        $newUser = new User();
        $newUser->fill([
            "email" => $allInput["email"],
            "nama" => $allInput["nama"],
            "password" => Hash::make($allInput["password"]),
        ]);
        $saved = $newUser->save();

        if ($saved) {
            return response()->json($newUser);
        } else {
            return $this->makeErrors(["Database error"], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if ($user) {
            return response()->json($user);
        } else {
            return $this->makeErrors(["Vendor not found"]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allInput = $request->all();
        $validator = $this->validateUser($allInput);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->makeErrors($errors);
        }

        $user = User::find($id);
        if (!empty($allInput["password"])) {
            $user->password = Hash::make($allInput["password"]);
        }
        $user->fill($allInput);
        $updated = $user->save();

        if ($updated) {
            return response()->json($user);
        } else {
            return $this->makeErrors(["Database Error"], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        if ($request->auth->role !== "admin") {
            return $this->makeErrors(["You don't have permission"]);
        }
        $user = User::find($id);
        if ($user) {
            $deleted = $user->delete();
            if ($deleted) {
                return response()->json($user);
            } else {
                return $this->makeErrors(["Database Error"], 500);
            }
        } else {
            return $this->makeErrors(["Vendor not found"]);
        }
    }
}
