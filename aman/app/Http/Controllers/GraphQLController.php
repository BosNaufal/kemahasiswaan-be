<?php

namespace App\Http\Controllers;

use GraphQL\GraphQL;
use GraphQL\Type\Schema;
use GraphQL\Error\Error;
use GraphQL\Error\FormattedError;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

use Illuminate\Http\Request;
use App\Http\Controllers\GraphQL\User\UserGraph;
use App\Http\Controllers\GraphQL\Beasiswa\BeasiswaGraph;

class GraphQLController extends Controller
{
    public function __construct()
    {
        $this->middleware('graphql.auth');
    }

    public function index(Request $request)
    {
        $batch = [];
        $counter = 0;
        $addBatch = function() use (&$batch, &$counter) {
            array_push($batch, $counter);
            $counter = $counter + 1;
        };
        $schema = new Schema([
            'query' => new ObjectType([
                'name' => 'Query',
                'fields' => [
                    'login' => UserGraph::loginSchema(),
                    'logout' => UserGraph::logoutSchema(),

                    'users' => UserGraph::userListSchema(),
                    'user' => UserGraph::userDetailSchema(),

                    'beasiswas' => BeasiswaGraph::beasiswaListSchema(),
                    'beasiswa' => BeasiswaGraph::beasiswaDetailSchema(),
                    
                    'story' => [
                        'type' => Type::listOf(new ObjectType([
                            'name' => 'StoryList',
                            'fields' => [
                                'id' => Type::string(),
                                'judul' => Type::string(),
                                'konten' => Type::string(),
                                'author' => [
                                    'type' => new ObjectType([
                                        'name' => 'Author',
                                        'fields' => [
                                            'id' => Type::string(),
                                            'name' => Type::string(),
                                        ],
                                    ]),
                                    'resolve' => function($root) use ($addBatch, &$batch) {
                                        $addBatch();
                                        return new \GraphQL\Deferred(function () use ($root, &$batch) {
                                            dump("author deffered");
                                            dump($batch);
                                            return [
                                                'id' => 'idnya',
                                                'name' => 'namenya',
                                            ];
                                        });
                                    },
                                ],
                            ],
                        ])),
                        'resolve' => function($root) {
                            // dump("story");
                            return [
                                'id' => 'idnya',
                                'judul' => 'judulnya',
                                'konten' => 'kontennya',
                            ];
                        }
                    ],
                    'echo' => [
                        'type' => Type::string(),
                        'args' => [
                            'message' => Type::nonNull(Type::string()),
                        ],
                        'resolve' => function ($root, $args) {
                            return $root['prefix'] . $args['message'];
                        }
                    ],
                ],
            ]),

            'mutation' => new ObjectType([
                'name' => 'Mutation',
                'fields' => [
                    'userCreate' => UserGraph::createUserSchema(),
                    'userUpdate' => UserGraph::updateUserSchema(),
                    'userDelete' => UserGraph::deleteUserSchema(),

                    'beasiswaCreate' => BeasiswaGraph::createBeasiswaSchema(),
                    'beasiswaUpdate' => BeasiswaGraph::updateBeasiswaSchema(),
                    'beasiswaDelete' => BeasiswaGraph::deleteBeasiswaSchema(),
                ],
            ]),
        ]);
        
        $rawInput = file_get_contents('php://input');
        $input = json_decode($rawInput, true);
        $query = $input['query'];
        $variableValues = isset($input['variables']) ? $input['variables'] : null;

        $myErrorFormatter = function(Error $error) {
            $path = $error->path[0];
            return FormattedError::createFromException($error, false, $error->message);
        };
        
        $myErrorHandler = function(array $errors, callable $formatter) {
            return array_map($formatter, $errors);
        };

        try {
            $rootValue = [
                'request' => $request,
                'prefix' => 'You said: '
            ];
            $result = GraphQL::executeQuery($schema, $query, $rootValue, null, $variableValues);
            $output = $result
                ->setErrorFormatter($myErrorFormatter)
                ->setErrorsHandler($myErrorHandler)
                ->toArray();



        } catch (\Exception $e) {
            dd($e);
            $output = [
                'errors' => [
                    [
                        'message' => $e->getMessage()
                    ]
                ]
            ];
        }
        return response()->json($output);
    }
}
