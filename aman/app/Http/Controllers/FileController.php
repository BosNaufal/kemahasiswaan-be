<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', [
            'only' => [
                "destroy",
                // "store"
            ]
        ]);
    }

    protected $dir = "uploaded";

    protected function getDisk() {
        return Storage::disk('local');
    }

    protected function makeErrors($messages, $code = 400) {
        return response()->json(["error" => true, "messages" => $messages], $code);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->hasFile('file')) {
            return $this->makeErrors(["No file uploaded"]);
        }
        $file = $request->file('file');
        if ($file->isValid()) {

            $mimeTypeSplitted = explode("/", $file->getMimeType());
            $ext = $file->extension();

            $makeName = function() use ($ext) {
                $fileName = Str::uuid();
                $fullFileName = $fileName . "." . $ext;
                return $fullFileName;
            };

            $uploadUnique = function() use ($makeName, $file) {
                $disk = $this->getDisk();
                $fullFileName = $makeName();
                $dir = $this->dir;
                $fullPath = $dir . "/" . $fullFileName;
                if (!$disk->exists($fullPath)) {
                    $file->storeAs($dir, $fullFileName);
                    return $fullFileName;
                } 
                return $uploadUnique();
            };

            $filename = $uploadUnique();
            $url = url("api/file/" . $filename);
            return response()->json([
                "url" => $url,
                "filename" => $filename,
            ], 200);
        } else {
            return $this->makeErrors(["File is not valid"]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($fileName)
    {
        $disk = $this->getDisk();
        $fullPath = $this->dir . "/" . $fileName;
        if (!$disk->exists($fullPath)) {
            return $this->makeErrors(["File not found"]);
        }
        return response()->file(storage_path("app/" . $fullPath));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($fileName)
    {
        $disk = $this->getDisk();
        $fullPath = $this->dir . "/" . $fileName;
        if (!$disk->exists($fullPath)) {
            return $this->makeErrors(["File not found"]);
        }
        $deleted = $disk->delete($fullPath);
        return $deleted;
    }
}
