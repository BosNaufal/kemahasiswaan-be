<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use PHPMailer\PHPMailer\PHPMailer;

use App\Pendaftar;
use App\Jawaban;

class PendaftarController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', [
            'only' => [
                "show",
                "destroy",
                "update",
                "logout",
            ]
        ]);
    }

    protected function makeErrors($messages, $code = 400) {
        return response()->json(["error" => true, "messages" => $messages], $code);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->query();

        $perPage = !empty($params['per_page']) 
            ? $params['per_page']
            : 10;
        
        $currentPage = !empty($params['page']) 
            ? $params['page']
            : 1;
        
        $beasiswaId = !empty($params['beasiswa_id']) 
            ? $params['beasiswa_id']
            : false;

        $status = !empty($params['status']) 
            ? $params['status']
            : false;

        $search = !empty($params['search']) 
            ? $params['search']
            : false;

        $pendaftarsQuery = Pendaftar::orderBy("created_at", "desc");

        if ($beasiswaId) {
            $pendaftarsQuery->where('beasiswa_id', $beasiswaId);
        }

        if ($status) {
            if ($status === 'null') {
                $pendaftarsQuery->where('status', null);
            } else {
                $pendaftarsQuery->where('status', $status);
            }
        } else {
            $pendaftarsQuery->where(function($query) {
                $query
                    ->where('status', '!=', "selesai")
                    ->orWhere('status', '=', null);
            });
        }

        if ($search) {
            $pendaftarsQuery->where(function ($query) use ($search) {
                $query
                    ->where('nama', "like", "%{$search}%")
                    ->orWhere('email', "like", "%{$search}%")
                    ->orWhere('gender', "like", "%{$search}%");
            });
        }

        $pendaftarsCount = $pendaftarsQuery->count();
        $pendaftars = $pendaftarsQuery
            ->limit($perPage)
            ->offset(($currentPage - 1) * $perPage)
            ->get();

        if ($pendaftars) {
            $meta = [
                "per_page" => $perPage,
                "page" => $currentPage,
                "total_data" => $pendaftarsCount
            ];
            return response()->json([
                "results" => $pendaftars,
                "meta" => $meta,
            ], 200, [], JSON_NUMERIC_CHECK);
        } else {
            return $this->makeErrors(["No user found"]);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //    
    }

    private function validatePendaftar($allInput, $isCreate = false) {
        $validator = Validator::make($allInput, [
            "beasiswa_id" => 'required',
            "nama" => 'required',
            "email" => 'required',
            "jawaban" => $isCreate ? 'required' : '',
        ]);
        return $validator;
    }

    public function umumkan(Request $request)
    {
        $allInput = $request->all();
        $validator = Validator::make($allInput, [
            "pesan" => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->makeErrors($errors);
        }

        $mail = new PHPMailer;

        // Konfigurasi SMTP
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'admiinPISI@gmail.com';
        $mail->Password = 'bunitaclub';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;

        $mail->setFrom('admiinPISI@gmail.com', 'Admin Beasiswa UINSA');
        $mail->addReplyTo('admiinPISI@gmail.com', 'Admin Beasiswa UINSA');

        // Menambahkan penerima
        foreach ($allInput['penerima'] as $penerima) {
            $mail->addAddress($penerima);
        }

        // Menambahkan cc atau bcc 
        // $mail->addCC('lailatulm185@gmail.com');
        // $mail->addBCC('bcc@contoh.com');

        // Subjek email
        $mail->Subject = 'Pengumuman Beasiswa UINSA';

        // Mengatur format email ke HTML
        $mail->isHTML(true);

        // Konten/isi email
        // $mailContent = "<h1>Mengirim Email HTML menggunakan SMTP di PHP</h1>
        //     <p>Ini adalah email percobaan yang dikirim menggunakan email server SMTP dengan PHPMailer.</p>";

        $mailContent = $allInput["pesan"];
        $mail->Body = $mailContent;

        // Kirim email
        if (!$mail->send()){
            // echo 'Pesan tidak dapat dikirim.';
            // echo 'Mailer Error: ' . $mail->ErrorInfo;
            return response()->json([
                "error" => true,
                "messages" => $mail->ErrorInfo
            ]);
        } else {
            // echo 'Pesan telah terkirim';
            return response()->json([
                "error" => false
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allInput = $request->all();
        $validator = $this->validatePendaftar($allInput, true);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->makeErrors($errors);
        }

        $emailFound = Pendaftar::
            where("email", $allInput["email"])
            ->where("beasiswa_id", $allInput["beasiswa_id"])
            ->count();
        
        if ($emailFound > 0) {
            return $this->makeErrors(["Email telah terdaftar pada beasiswa ini"], 400);
        }

        $newPendaftar = new Pendaftar();
        $newPendaftar->fill($allInput);

        $saved = $newPendaftar->save();
        if (!$saved) return $this->makeErrors(["Database error"], 500);

        $allJawaban = json_decode($allInput['jawaban']);
        $allJawabanModel = [];
        foreach ($allJawaban as $jawaban) {
            $jawabanArray = [];
            $jawabanArray["id"] = (string) Str::uuid();
            $jawabanArray["pendaftar_id"] = $newPendaftar->id;
            $jawabanArray["created_at"] = new \DateTime();
            $jawabanArray["updated_at"] = $jawabanArray["created_at"];
            $jawabanArray['question_id'] = $jawaban->question_id;
            $jawabanArray['question'] = $jawaban->question;
            $jawabanArray['required'] = $jawaban->required;
            $jawabanArray['type'] = $jawaban->type;
            $jawabanArray['value'] = $jawaban->value;
            $jawabanArray['answers'] = json_encode($jawaban->answers);
            array_push($allJawabanModel, $jawabanArray);
        }

        $jawabanSaved = DB::table('jawabans')->insert($allJawabanModel);
        if (!$jawabanSaved) return $this->makeErrors(["Database error"], 500);

        $newPendaftar["jawabans"] = $newPendaftar->jawabans;
        return response()->json($newPendaftar);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pendaftar = Pendaftar::with('beasiswa')
            ->with('jawabans')
            ->find($id);
        if ($pendaftar) {
            return response()->json($pendaftar);
        } else {
            return $this->makeErrors(["Pendaftar not found"]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allInput = $request->all();
        $validator = $this->validatePendaftar($allInput);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->makeErrors($errors);
        }

        $pendaftar = Pendaftar::find($id);
        if (!$pendaftar) return $this->makeErrors(["Pendaftar Not Found"], 500);

        if (!empty($allInput["password"])) {
            $pendaftar->password = Hash::make($allInput["password"]);
        }
        $pendaftar->fill($allInput);
        $updated = $pendaftar->save();

        if ($updated) {
            return response()->json($pendaftar);
        } else {
            return $this->makeErrors(["Database Error"], 500);
        }
    }

    public function bulkUpdateStatus(Request $request, $beasiswaId) 
    {
        $pendaftarTable = (new Pendaftar())->getTable();
        // https://stackoverflow.com/questions/22430716/eloquent-model-mass-update
        $updated = DB::table($pendaftarTable)
            ->where('beasiswa_id', $beasiswaId)
            ->where('status', '!=', 'selesai')
            ->update(array("status" => 'selesai'));
        if ($updated) {
            return response()->json($updated);
        } else {
            return $this->makeErrors(["Database Error"], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $pendaftar = Pendaftar::find($id);
        if ($pendaftar) {
            $deleted = $pendaftar->delete();
            if ($deleted) {
                return response()->json($pendaftar);
            } else {
                return $this->makeErrors(["Database Error"], 500);
            }
        } else {
            return $this->makeErrors(["Pendaftar not found"]);
        }
    }
}
