<?php

namespace App\Http\Controllers;

use Validator;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Beasiswa;
use App\Pendaftar;

class BeasiswaController extends Controller
{

    public function __construct()
    {
        DB::enableQueryLog();
        $this->middleware('auth', [
            'only' => [
                "store",
                "destroy",
                "update",
                "logout",
            ]
        ]);
    }

    protected function makeErrors($messages, $code = 400) {
        return response()->json(["error" => true, "messages" => $messages], $code);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->query();

        $perPage = !empty($params['per_page']) 
            ? $params['per_page']
            : 10;
        
        $currentPage = !empty($params['page']) 
            ? $params['page']
            : 1;

        $search = !empty($params['search']) 
            ? $params['search']
            : false;

        $status = !empty($params['status']) 
            ? $params['status']
            : false;

        $beasiswasQuery = Beasiswa::orderBy("created_at", "desc");

        if ($status) {
            if ($status !== "all") {
                $logicSign = $status === "published" ? '!=' : '=';
                $beasiswasQuery->where('published_date', $logicSign, null);
            }
        }

        if ($search) {
            $beasiswasQuery->where(function($query) use ($search) {
                $query
                    ->where('judul', 'like', "%{$search}%")
                    ->orWhere('konten', 'like', "%{$search}%");
            });
        }
        
        $beasiswasCount = $beasiswasQuery->count();
        $beasiswas = $beasiswasQuery
            ->limit($perPage)
            ->offset(($currentPage - 1) * $perPage)
            ->get();

        if ($beasiswas) {
            $meta = [
                "per_page" => $perPage,
                "page" => $currentPage,
                "total_data" => $beasiswasCount
            ];
            return response()->json([
                "results" => $beasiswas,
                "meta" => $meta,
            ]);
        } else {
            return $this->makeErrors(["No Beasiswa found"]);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //    
    }

    private function validateBeasiswa($allInput, $isCreate = false) {
        $validator = Validator::make($allInput, [
            "judul" => 'required',
            "konten" => 'required',
        ]);
        return $validator;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allInput = $request->all();
        $validator = $this->validateBeasiswa($allInput, true);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->makeErrors($errors);
        }

        $newBeasiswa = new Beasiswa();
        $newBeasiswa->fill($allInput);
        $saved = $newBeasiswa->save();

        if ($saved) {
            return response()->json($newBeasiswa);
        } else {
            return $this->makeErrors(["Database error"], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $beasiswa = Beasiswa::find($id);
        if ($beasiswa) {
            $pendaftarBeasiswa = Pendaftar::where("beasiswa_id", $id);
            $approvedPendaftarCount = $pendaftarBeasiswa->where('status', "diterima")->count();
            $beasiswa->has_approved_pendaftar = $approvedPendaftarCount > 0;
            return response()->json($beasiswa);
        } else {
            return $this->makeErrors(["Beasiswa not found"]);
        }
    }

    public function scoreboard(Request $request)
    {
        $params = $request->query();

        $perPage = !empty($params['per_page']) 
            ? $params['per_page']
            : 10;
        
        $currentPage = !empty($params['page']) 
            ? $params['page']
            : 1;

        $beasiswasQuery = Beasiswa::select("id", "judul")
            ->has("pendaftars")
            ->withCount([
                'pendaftars as all_pendaftar', 
                'pendaftars as pendaftar_aktif' => function ($query) {
                    $query
                        ->where('status', '!=', "selesai")
                        ->orWhere('status', null);
                },
                'pendaftars as pendaftar_selesai' => function ($query) {
                    $query->where('status', "selesai");
                },
            ])
            ->orderBy("pendaftar_aktif", "desc")
            ->orderBy("created_at", "desc");
        
        
        $beasiswasCount = $beasiswasQuery->count();
        $beasiswas = $beasiswasQuery
            ->limit($perPage)
            ->offset(($currentPage - 1) * $perPage)
            ->get();

        if ($beasiswas) {
            $meta = [
                "per_page" => $perPage,
                "page" => $currentPage,
                "total_data" => $beasiswasCount
            ];
            return response()->json([
                "results" => $beasiswas,
                "meta" => $meta,
            ]);
        } else {
            return $this->makeErrors(["No Beasiswa found"]);
        }
    }

    public function analytic(Request $request)
    {
        $allInput = $request->all();
        $validator = Validator::make($allInput, [
            "beasiswas" => 'required',
            "years" => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->makeErrors($errors);
        }

        $years = $allInput["years"];
        $analytics = Beasiswa::with(['pendaftars' => 
                function($pendaftar) use ($years) {
                    $pendaftarInRangeQuery = $pendaftar
                    ->select(["id", "beasiswa_id", "gender", "last_status", "created_at"])
                    ->where(function($query) use ($years) {
                        for ($i=0; $i < sizeof($years); $i++) { 
                            $year = $years[$i];
                            if ($i === 0) {
                                $query->where(function ($sub) use ($year) {
                                    $sub
                                        ->whereDate('created_at', '>=', "{$year}-01-01")
                                        ->whereDate('created_at', '<=', "{$year}-12-31");
                                });
                            } else {
                                $query->orWhere(function ($sub) use ($year) {
                                    $sub
                                        ->whereDate('created_at', '>=', "{$year}-01-01")
                                        ->whereDate('created_at', '<=', "{$year}-12-31");
                                });
                            }
                        }
                    });
                }
            ])
            ->select(["id", "judul"])
            ->whereIn("id", $allInput["beasiswas"])
            ->get();
            $results = $analytics;
            $results = $analytics->map(function ($beasiswa) {
                $pendaftars = $beasiswa->pendaftars->map(function($pendaftar) {
                    $pendaftar->tahun = $pendaftar->created_at->format('Y');
                    return $pendaftar;
                });
                $beasiswa->pendaftars = $pendaftars;
                unset($beasiswa->pendaftars);

                $pendaftarByTahun = $pendaftars->groupBy('tahun');
                $tahuns = $pendaftarByTahun
                    ->keys()
                    ->map(function($tahun) use ($pendaftarByTahun) {
                        $pendaftarTahunan = $pendaftarByTahun->get($tahun);
                        return [
                            "tahun" => $tahun,
                            "gender" => [
                                "laki-laki" => $pendaftarTahunan->where('gender', 'laki-laki')->count(),
                                "perempuan" => $pendaftarTahunan->where('gender', 'perempuan')->count(),
                            ],
                            "jumlah" => $pendaftarByTahun->count(),
                            "status" => [
                                "diterima" => $pendaftarTahunan->where('last_status', 'diterima')->count(),
                                "ditolak" => $pendaftarTahunan->where('last_status', 'ditolak')->count(),
                                "unconfirmed" => $pendaftarTahunan->where('status', null)->count(),
                            ],
                            "status_and_gender" => [
                                "diterima_laki_laki" => $pendaftarTahunan->where('last_status', 'diterima')->where("gender", "laki-laki")->count(),
                                "diterima_perempuan" => $pendaftarTahunan->where('last_status', 'diterima')->where("gender", "perempuan")->count(),

                                "ditolak_laki_laki" => $pendaftarTahunan->where('last_status', 'ditolak')->where("gender", "laki-laki")->count(),
                                "ditolak_perempuan" => $pendaftarTahunan->where('last_status', 'ditolak')->where("gender", "perempuan")->count(),
                                
                                "unconfirmed_laki_laki" => $pendaftarTahunan->where('status', null)->where("gender", "laki-laki")->count(),
                                "unconfirmed_perempuan" => $pendaftarTahunan->where('status', null)->where("gender", "perempuan")->count(),
                            ],
                        ];
                        return $tahun; 
                    });
                $beasiswa->analytics = $tahuns;
                return $beasiswa;
            });
            // https://stackoverflow.com/a/51777447
            // function getEloquentSqlWithBindings($query)
            // {
            //     return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            //         return is_numeric($binding) ? $binding : "'{$binding}'";
            //     })->toArray());
            // }
            // dd(getEloquentSqlWithBindings($analytics));
        if ($results) {
            return response()->json($results);
        } else {
            return $this->makeErrors(["Analytics Wrong"]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allInput = $request->all();
        $validator = $this->validateBeasiswa($allInput);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->makeErrors($errors);
        }

        $beasiswa = Beasiswa::find($id);
        if (!empty($allInput["password"])) {
            $beasiswa->password = Hash::make($allInput["password"]);
        }
        $beasiswa->fill($allInput);
        $updated = $beasiswa->save();

        if ($updated) {
            return response()->json($beasiswa);
        } else {
            return $this->makeErrors(["Database Error"], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $beasiswa = Beasiswa::find($id);
        if ($beasiswa) {
            $deleted = $beasiswa->delete();
            if ($deleted) {
                return response()->json($beasiswa);
            } else {
                return $this->makeErrors(["Database Error"], 500);
            }
        } else {
            return $this->makeErrors(["Beasiswa not found"]);
        }
    }
}
