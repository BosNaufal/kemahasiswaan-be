<?php

namespace App\Http\Controllers\GraphQL\User;

use GraphQL\Error\Error;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

use App\Http\Controllers\GraphQL\Commons;


use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Auth;

class UserResolver {
  
  public static function userList() {
    return function($root, $args) {
        $perPage = !empty($args['per_page']) 
            ? $args['per_page']
            : 10;
        
        $currentPage = !empty($args['page']) 
            ? $args['page']
            : 1;

        $usersQuery = User::orderBy("created_at", "desc");
        
        $usersCount = $usersQuery->count();
        $users = $usersQuery
            ->limit($perPage)
            ->offset($currentPage - 1)
            ->get();

        if ($users) {
            $meta = [
                "per_page" => $perPage,
                "page" => $currentPage,
                "total_data" => $usersCount
            ];
            return [
                "results" => $users,
                "meta" => $meta,
            ];
        } else {
            return [
                "error" => true,
                "error_messages" => ["No User found"],
            ];
        }
    };
  }

  public static function userDetail() {
    return function($root, $args) {
        $user = User::find($args['id']);
        if ($user) {
            return ["node" => $user];
        } else {
            return [
                "error" => true,
                "error_messages" => ["No User Found"]
            ];
        }
    };
  }

  public static function login() {
      return function($root, $args) {
        $user = User::where("email", $args["email"])
            ->first();
        
        if ($user) {
            $validPassword = Hash::check($args["password"], $user->password);
            if ($validPassword) {
                $auth = Auth::where("user_id", $user->id)->first();
                if (!$auth) {
                    $auth = new Auth();
                    $auth->user_id = $user->id;
                    $auth->save();
                }
                $token = $auth->token;
                $user->token = $token;
                return [
                    "token" => $auth->token,
                    "user" => $user
                ];
            } else {
                return [
                    "error" => true,
                    "error_messages" => ["Password is invalid"],
                ];
            }
        } else {
            return [
                "error" => true,
                "error_messages" => ["User not found"],
            ];
        }
      };
  }

  public static function logout() {
    return function($root, $args) {
        $req = $root["request"];
        if (!$req->auth) {
            return [
                "error" => true,
                "error_messages" => ["Token not supplied"],
            ];
        }
        $token = $req->auth->token;
        $auth = Auth::find($token);
        if ($auth) {
            $deleted = $auth->delete();
            if ($deleted) {
                return [ "error" => false ];
            } else {
                return [
                    "error" => true,
                    "error_messages" => ["Database Error"],
                ];
            }
        } else {
            return [
                "error" => true,
                "error_messages" => ["Token not found"],
            ];
        }
    };
 }

 private static function validateUser($input, $isCreate = false) {
    $validator = Validator::make($input, [
        "email" => $isCreate ? 'required|unique:users' : '',
        "nama" => $isCreate ? 'required' : '',
        "password" => $isCreate ? 'required' : '',
    ]);
    return $validator;
}

 public static function createUser()
 {
     return function ($root, $args)
     {
        $input = $args['user'];
        $validator = self::validateUser($input, true);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return [
                "error" => true,
                "error_messages" => $errors,
            ];
        }

        $newUser = new User();
        $newUser->fill([
            "email" => $input["email"],
            "nama" => $input["nama"],
            "password" => Hash::make($input["password"]),
        ]);
        $saved = $newUser->save();

        if ($saved) {
            return [ "user" => $newUser ];
        } else {
            return [
                "error" => true,
                "error_messages" => ["Database Error"],
            ];
        }
     };
 }

 public static function updateUser()
  {
      return function ($root, $args) {
        $input = $args["user"];
        $validator = self::validateUser($input, false);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return [
                "error" => true,
                "error_messages" => $errors,
            ];
        }

        $user = User::find($args['id']);
        $user->fill($input);

        if (!empty($input["password"])) {
            $user->password = Hash::make($input["password"]);
        }
        
        $updated = $user->save();

        if ($updated) {
            return [ "user" => $user ];
        } else {
            return [
                "error" => true,
                "error_messages" => ["Database Error"]
            ];
        }
      };
  }

  public static function deleteUser()
  {
      return function ($root, $args) {
        $user = User::find($args['id']);
        if ($user) {
            $deleted = $user->delete();
            if ($deleted) {
                return [ "deleted" => true ];
            } else {
                return [
                    "error" => true,
                    "error_messages" => ["Database Error"]
                ];
            }
        } else {
            return [
                "error" => true,
                "error_messages" => ["User not found"]
            ];
        }
      };
  }
}



