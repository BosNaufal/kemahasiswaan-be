<?php

namespace App\Http\Controllers\GraphQL\User;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
// use GraphQL\Type\Definition\UnionType;

use App\Http\Controllers\GraphQL\Commons;
use App\Http\Controllers\GraphQL\User\UserResolver;

class UserGraph {
  static $userType;
  static $userInputType;
  static $userUpdateInputType;

  public static function userListSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'UserList',
        'fields' => [
            'results' => Type::listOf(self::$userType),
            'meta' => Commons::$metaListType,

            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'page' => Type::int(),
          'per_page' => Type::int(),
      ],
      'resolve' => UserResolver::userList(),
    ];
  }

  public static function userDetailSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'UserDetail',
        'fields' => [
            'node' => self::$userType,
            
            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'id' => Type::nonNull(Type::string()),
      ],
      'resolve' => UserResolver::userDetail(),
    ];
  }

  public static function loginSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'LoginResponse',
        'fields' => [
            'token' => Type::string(),
            'user' => self::$userType,
            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'email' => Type::nonNull(Type::string()),
          'password' => Type::nonNull(Type::string()),
      ],
      'resolve' => UserResolver::login(),
    ];
  }

  public static function logoutSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'logoutResponse',
        'fields' => [
          "error" => Type::boolean(),
          "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'resolve' => UserResolver::logout(),
    ];
  }

  public static function createUserSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'CreateUser',
        'fields' => [
            'user' => self::$userType,

            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'user' => self::$userInputType,
      ],
      'resolve' => UserResolver::createUser(),
    ];
  }


  public static function updateUserSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'UpdateUser',
        'fields' => [
            'user' => self::$userType,

            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'id' => Type::nonNull(Type::string()),
          'user' => self::$userUpdateInputType,
      ],
      'resolve' => UserResolver::updateUser(),
    ];
  }


  public static function deleteUserSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'deleteUser',
        'fields' => [
            'deleted' => Type::boolean(),
            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'id' => Type::nonNull(Type::string()),
      ],
      'resolve' => UserResolver::deleteUser(),
    ];
  }
}

UserGraph::$userType = new ObjectType([
  'name' => 'User',
  'fields' => [
      "id" => Type::string(),
      "email" => Type::string(),
      "nama" => Type::string(),
      "created_at" => Type::string(),
      "updated_at" => Type::string(),
      "token" => Type::string(),
  ]
]);

UserGraph::$userInputType = new InputObjectType([
  'name' => 'UserInput',
  'fields' => [
      "email" => Type::nonNull(Type::string()),
      "nama" => Type::nonNull(Type::string()),
      "password" => Type::nonNull(Type::string()),
  ]
]);

UserGraph::$userUpdateInputType = new InputObjectType([
  'name' => 'UserUpdateInput',
  'fields' => [
      "email" => Type::string(),
      "nama" => Type::string(),
      "password" => Type::string(),
  ]
]);



