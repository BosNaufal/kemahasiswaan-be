<?php

namespace App\Http\Controllers\GraphQL\Beasiswa;

use GraphQL\Error\Error;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

use App\Http\Controllers\GraphQL\Commons;


use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Beasiswa;
use App\Auth;

class BeasiswaResolver {
  
  public static function beasiswaList() {
    $batch = [];
    $hasLoaded = [];
    $addBatch = function($info) use (&$batch, &$counter) {
        array_push($batch, $info);
    };
    return function($root, $args, $context) use ($addBatch) {
        $context["batch"] = $addBatch;
        $perPage = !empty($args['per_page']) 
            ? $args['per_page']
            : 10;
        
        $currentPage = !empty($args['page']) 
            ? $args['page']
            : 1;

        $beasiswasQuery = Beasiswa::orderBy("created_at", "desc");
        
        $beasiswasCount = $beasiswasQuery->count();
        $beasiswas = $beasiswasQuery
            ->limit($perPage)
            ->offset($currentPage - 1)
            ->get();

        if ($beasiswas) {
            $meta = [
                "per_page" => $perPage,
                "page" => $currentPage,
                "total_data" => $beasiswasCount
            ];
            return [
                "results" => $beasiswas,
                "meta" => $meta,
            ];
        } else {
            return [
                "error" => true,
                "error_messages" => ["No Beasiswa found"],
            ];
        }
    };
  }

  public static function beasiswaDetail() {
    return function($root, $args) {
        $beasiswa = Beasiswa::find($args['id']);
        if ($beasiswa) {
            return ["node" => $beasiswa];
        } else {
            return [
                "error" => true,
                "error_messages" => ["No Beasiswa Found"]
            ];
        }
    };
  }

  public static function createBeasiswa()
  {
      return function ($root, $args) {
        $newBeasiswa = new Beasiswa();
        $newBeasiswa->fill($args["beasiswa"]);
        $saved = $newBeasiswa->save();

        if ($saved) {
            return [ "beasiswa" => $newBeasiswa ];
        } else {
            return [
                "error" => true,
                "error_messages" => ["Database Error"]
            ];
        }
      };
  }

  public static function updateBeasiswa()
  {
      return function ($root, $args) {
        $beasiswa = Beasiswa::find($args['id']);

        $input = $args['beasiswa'];

        $beasiswa->fill($input);
        $updated = $beasiswa->save();

        if ($updated) {
            return [ "beasiswa" => $beasiswa ];
        } else {
            return [
                "error" => true,
                "error_messages" => ["Database Error"]
            ];
        }
      };
  }

  public static function deleteBeasiswa()
  {
      return function ($root, $args) {
        $beasiswa = Beasiswa::find($args['id']);
        if ($beasiswa) {
            $deleted = $beasiswa->delete();
            if ($deleted) {
                return [ "deleted" => true ];
            } else {
                return [
                    "error" => true,
                    "error_messages" => ["Database Error"]
                ];
            }
        } else {
            return [
                "error" => true,
                "error_messages" => ["Beasiswa not found"]
            ];
        }
      };
  }
  
}



