<?php

namespace App\Http\Controllers\GraphQL\Beasiswa;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\EnumType;
// use GraphQL\Type\Definition\UnionType;

use App\Http\Controllers\GraphQL\Commons;
use App\Http\Controllers\GraphQL\Beasiswa\BeasiswaResolver;

class BeasiswaGraph {
  static $beasiswaType;
  static $beasiswaInputType;
  static $beasiswaInputUpdateType;

  public static function beasiswaListSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'BeasiswaList',
        'fields' => [
            'results' => [
              'type' => Type::listOf(self::$beasiswaType),
            ],
            'meta' => Commons::$metaListType,

            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'page' => Type::int(),
          'per_page' => Type::int(),
      ],
      'resolve' => BeasiswaResolver::beasiswaList(),
    ];
  }

  public static function beasiswaDetailSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'BeasiswaDetail',
        'fields' => [
            'node' => self::$beasiswaType,
            
            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'id' => Type::nonNull(Type::string()),
      ],
      'resolve' => BeasiswaResolver::beasiswaDetail(),
    ];
  }

  public static function createBeasiswaSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'CreateBeasiswa',
        'fields' => [
            'beasiswa' => self::$beasiswaType,

            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'beasiswa' => self::$beasiswaInputType,
      ],
      'resolve' => BeasiswaResolver::createBeasiswa(),
    ];
  }


  public static function updateBeasiswaSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'UpdateBeasiswa',
        'fields' => [
            'beasiswa' => self::$beasiswaType,

            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'id' => Type::nonNull(Type::string()),
          'beasiswa' => self::$beasiswaInputUpdateType,
      ],
      'resolve' => BeasiswaResolver::updateBeasiswa(),
    ];
  }


  public static function deleteBeasiswaSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'deleteBeasiswa',
        'fields' => [
            'deleted' => Type::boolean(),
            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'id' => Type::nonNull(Type::string()),
      ],
      'resolve' => BeasiswaResolver::deleteBeasiswa(),
    ];
  }

}

BeasiswaGraph::$beasiswaType = new ObjectType([
  'name' => 'Beasiswa',
  'fields' => [
      "id" => Type::string(),
      "judul" => Type::string(),
      "published_date" => Type::string(),
      "email_body" => Type::string(),
      "konten" => Type::string(),
      "form" => Type::string(),
      "created_at" => Type::string(),
      "updated_at" => Type::string(),

      'pendaftar' => [
        'type' => new ObjectType([
          'name' => 'Pendaftar',
          'fields' => [
            "beasiswa_id" => Type::string(),
            "nama" => Type::string(),
            "email" => Type::string(),
            "status" => new EnumType([
              'name' => 'StatusPendaftar',
              'values' => [
                'NEWHOPE' => [
                  'value' => 4,
                  'description' => 'Released in 1977.'
                ],
                'EMPIRE' => [
                  'value' => 5,
                  'description' => 'Released in 1980.'
                ],
              ]
            ]),
            "jawaban" => Type::string(),
            "created_at" => Type::string(),
            "updated_at" => Type::string(),
          ],
        ]),
        'resolve' => function($beasiswa, $args, $context) {
          dd($context);
          return "halo";
        },
      ],
  ],
]);

BeasiswaGraph::$beasiswaInputType = new InputObjectType([
  'name' => 'BeasiswaInput',
  'fields' => [
      "judul" => Type::nonNull(Type::string()),
      "konten" => [
        "type" => Type::nonNull(Type::string()),
        "description" => "HTML String for content"
      ],
      "published_date" => Type::string(),
      "email_body" => Type::string(),
      "form" => [
        "type" => Type::string(),
        "description" => "JSON string from form builder"
      ],
  ]
]);

BeasiswaGraph::$beasiswaInputUpdateType = new InputObjectType([
  'name' => 'BeasiswaUpdateInput',
  'fields' => [
      "judul" => Type::string(),
      "konten" => [
        "type" => Type::string(),
        "description" => "HTML String for content"
      ],
      "published_date" => Type::string(),
      "email_body" => Type::string(),
      "form" => [
        "type" => Type::string(),
        "description" => "JSON string from form builder"
      ],
  ]
]);



