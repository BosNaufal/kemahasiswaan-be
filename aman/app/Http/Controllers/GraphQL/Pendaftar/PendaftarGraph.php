<?php

namespace App\Http\Controllers\GraphQL\Pendaftar;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;
// use GraphQL\Type\Definition\UnionType;

use App\Http\Controllers\GraphQL\Commons;
use App\Http\Controllers\GraphQL\Pendaftar\PendaftarResolver;

class PendaftarGraph {
  static $pendaftarType;
  static $pendaftarInputType;
  static $pendaftarInputUpdateType;

  public static function pendaftarListSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'PendaftarList',
        'fields' => [
            'results' => Type::listOf(self::$pendaftarType),
            'meta' => Commons::$metaListType,

            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'page' => Type::int(),
          'per_page' => Type::int(),
      ],
      'resolve' => PendaftarResolver::pendaftarList(),
    ];
  }

  public static function pendaftarDetailSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'PendaftarDetail',
        'fields' => [
            'node' => self::$pendaftarType,
            
            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'id' => Type::nonNull(Type::string()),
      ],
      'resolve' => PendaftarResolver::pendaftarDetail(),
    ];
  }

  public static function createPendaftarSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'CreatePendaftar',
        'fields' => [
            'pendaftar' => self::$pendaftarType,

            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'pendaftar' => self::$pendaftarInputType,
      ],
      'resolve' => PendaftarResolver::createPendaftar(),
    ];
  }


  public static function updatePendaftarSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'UpdatePendaftar',
        'fields' => [
            'pendaftar' => self::$pendaftarType,

            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'id' => Type::nonNull(Type::string()),
          'pendaftar' => self::$pendaftarInputUpdateType,
      ],
      'resolve' => PendaftarResolver::updatePendaftar(),
    ];
  }


  public static function deletePendaftarSchema() {
    return [
      'type' => new ObjectType([
        'name' => 'deletePendaftar',
        'fields' => [
            'deleted' => Type::boolean(),
            "error" => Type::boolean(),
            "error_messages" => Type::listOf(Type::string()),
        ]
      ]),
      'args' => [
          'id' => Type::nonNull(Type::string()),
      ],
      'resolve' => PendaftarResolver::deletePendaftar(),
    ];
  }

}

PendaftarGraph::$pendaftarType = new ObjectType([
  'name' => 'Pendaftar',
  'fields' => [
      "id" => Type::string(),
      "judul" => Type::string(),
      "published_date" => Type::string(),
      "email_body" => Type::string(),
      "konten" => Type::string(),
      "form" => Type::string(),
      "created_at" => Type::string(),
      "updated_at" => Type::string(),
  ]
]);

PendaftarGraph::$pendaftarInputType = new InputObjectType([
  'name' => 'PendaftarInput',
  'fields' => [
      "judul" => Type::nonNull(Type::string()),
      "konten" => [
        "type" => Type::nonNull(Type::string()),
        "description" => "HTML String for content"
      ],
      "published_date" => Type::string(),
      "email_body" => Type::string(),
      "form" => [
        "type" => Type::string(),
        "description" => "JSON string from form builder"
      ],
  ]
]);

PendaftarGraph::$pendaftarInputUpdateType = new InputObjectType([
  'name' => 'PendaftarUpdateInput',
  'fields' => [
      "judul" => Type::string(),
      "konten" => [
        "type" => Type::string(),
        "description" => "HTML String for content"
      ],
      "published_date" => Type::string(),
      "email_body" => Type::string(),
      "form" => [
        "type" => Type::string(),
        "description" => "JSON string from form builder"
      ],
  ]
]);



