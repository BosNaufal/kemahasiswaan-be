<?php

namespace App\Http\Controllers\GraphQL\Pendaftar;

use GraphQL\Error\Error;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

use App\Http\Controllers\GraphQL\Commons;


use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Pendaftar;
use App\Auth;

class PendaftarResolver {
  
  public static function pendaftarList() {
    return function($root, $args) {
        $perPage = !empty($args['per_page']) 
            ? $args['per_page']
            : 10;
        
        $currentPage = !empty($args['page']) 
            ? $args['page']
            : 1;

        $pendaftarsQuery = Pendaftar::orderBy("created_at", "desc");
        
        $pendaftarsCount = $pendaftarsQuery->count();
        $pendaftars = $pendaftarsQuery
            ->limit($perPage)
            ->offset($currentPage - 1)
            ->get();

        if ($pendaftars) {
            $meta = [
                "per_page" => $perPage,
                "page" => $currentPage,
                "total_data" => $pendaftarsCount
            ];
            return [
                "results" => $pendaftars,
                "meta" => $meta,
            ];
        } else {
            return [
                "error" => true,
                "error_messages" => ["No Pendaftar found"],
            ];
        }
    };
  }

  public static function pendaftarDetail() {
    return function($root, $args) {
        $pendaftar = Pendaftar::find($args['id']);
        if ($pendaftar) {
            return ["node" => $pendaftar];
        } else {
            return [
                "error" => true,
                "error_messages" => ["No Pendaftar Found"]
            ];
        }
    };
  }

  public static function createPendaftar()
  {
      return function ($root, $args) {
        $newPendaftar = new Pendaftar();
        $newPendaftar->fill($args["pendaftar"]);
        $saved = $newPendaftar->save();

        if ($saved) {
            return [ "pendaftar" => $newPendaftar ];
        } else {
            return [
                "error" => true,
                "error_messages" => ["Database Error"]
            ];
        }
      };
  }

  public static function updatePendaftar()
  {
      return function ($root, $args) {
        $pendaftar = Pendaftar::find($args['id']);

        $input = $args['pendaftar'];

        $pendaftar->fill($input);
        $updated = $pendaftar->save();

        if ($updated) {
            return [ "pendaftar" => $pendaftar ];
        } else {
            return [
                "error" => true,
                "error_messages" => ["Database Error"]
            ];
        }
      };
  }

  public static function deletePendaftar()
  {
      return function ($root, $args) {
        $pendaftar = Pendaftar::find($args['id']);
        if ($pendaftar) {
            $deleted = $pendaftar->delete();
            if ($deleted) {
                return [ "deleted" => true ];
            } else {
                return [
                    "error" => true,
                    "error_messages" => ["Database Error"]
                ];
            }
        } else {
            return [
                "error" => true,
                "error_messages" => ["Pendaftar not found"]
            ];
        }
      };
  }
  
}



