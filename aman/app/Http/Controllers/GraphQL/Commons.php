<?php

namespace App\Http\Controllers\GraphQL;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class Commons {
  public static $metaListType;
  public static $errorType;
}


Commons::$metaListType = new ObjectType([
  'name' => 'MetaList',
  'fields' => [
      "per_page" => Type::int(),
      "page" => Type::int(),
      "total_data" => Type::int(),
  ]
]);

Commons::$errorType = new ObjectType([
  'name' => 'Error',
  'fields' => [
      "messages" => Type::listOf(Type::string()),
  ]
]);


