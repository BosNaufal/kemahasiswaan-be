<?php

namespace App\Http\Middleware;

use Closure;

class GraphQLAuth
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tokenParams = $request->query('token');
        $tokenHeader = $request->header("token");
        $token = isset($tokenParams) ? $tokenParams : $tokenHeader;
        
        $auth = \App\Auth::find($token);
        if ($auth) {
            $user = $auth->user;
            if ($user) {
                $user->token = $token;
                $request->auth = $user;
            } else {
                $request->auth = false;
            }
        } else {
            $request->auth = false;
        }
        return $next($request);
    }
}
