<?php

namespace App\Http\Middleware;

use Closure;

class Auth
{

    protected function makeErrors($messages, $code = 400) {
        return response()->json(["error" => true, "messages" => $messages], $code);
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header("token");
        $auth = \App\Auth::find($token);
        if ($auth) {
            $user = $auth->user;
            if ($user) {
                $user->token = $token;
                $request->auth = $user;
                return $next($request);
            } else {
                return $this->makeErrors(["User auth not found"]);
            }
        } else {
            return $this->makeErrors(["You don't have permission"]);
        }
        return $next($request);
    }
}
