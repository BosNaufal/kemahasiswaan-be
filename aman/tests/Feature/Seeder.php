<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Generator as Faker;
use GuzzleHttp;

use App\User;
use App\Category;

class TopAdsListingByTime extends TestCase
{
    // use RefreshDatabase;

    protected function createAds($payload) {
        $reqPayload = [
            "title" => $payload["title"],
            "category_id" => $payload["category_id"],
            "sub_category_id" => $payload["sub_category_id"],

            "images" => ["imageUrl", "imageUrl2", "imageUrl3"],
            "description" => "Vendor",
            "address" => "Address",
            "location_code" => "35.78.02",
            "start_price" => 500000,
            "end_price" => null,
            "price_info" => "price info"
        ];

        $guzzle = $this->prepareGuzzle();
        $response = $guzzle->request('POST', 'ads', [
            "headers" => [
                "token" => $this->vendorToken,
            ],
            "json" => $reqPayload
        ]);

        $data = json_decode($response->getBody()); 
        return $data;
    }

    protected function createManyAds($count, $basePayload) 
    {
        $allAds = [];
        for ($i = 0; $i < $count; $i++) {
            $ads = $this->createAds([
                "title" => "ads - " . ($i + 1), 
                "category_id" => $basePayload["category_id"], 
                "sub_category_id" => $basePayload["sub_category_id"]
            ]);
            array_push($allAds, $ads);
        }
        return collect($allAds);
    }

    protected function addAdsToQueue($ads, $time = [0, 1], $returnResponse = false) {
        $reqPayload = [
            "ads_id" => $ads->id,
            "category_id" => $ads->category->id,
            "sub_category_id" => $ads->sub_category->id,
            "start_time" => $time[0],
            "end_time" => $time[1],
        ];
        
        $guzzle = $this->prepareGuzzle();
        $response = $guzzle->request('POST', 'topads/queue', [
            "headers" => [
                "token" => $this->vendorToken,
            ],
            "json" => $reqPayload,
            "http_errors" => false
        ]);

        $data = json_decode($response->getBody()); 
        if ($returnResponse) {
            return $response;
        }
        return $data;
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTopAdsSlotLimit()
    {
        $slotLimit = 3;

        $this->makeAdminToken();

        $wedding = $this->createCategory([
            "name" => "wedding",
            "parent_id" => null,
        ]);

        $subWedding = [
            "gedung",
            "dekorasi",
            "catering",
            "souvenir",
            "undangan",
            "baju pengantin",
            "sound system",
            "make up",
            "tenda",
            "fotografi",
            "transportasi",
        ];

        foreach ($subWedding as $name) {
            $this->createCategory([
                "name" => $name,
                "parent_id" => $wedding->id,
            ]);
        }


        $seminar = $this->createCategory([
            "name" => "seminar",
            "parent_id" => null,
        ]);

        $subSeminar = [
            "gedung",
            "dekorasi",
            "sound system",
            "catering",
            "transportasi",
        ];

        foreach ($subSeminar as $name) {
            $this->createCategory([
                "name" => $name,
                "parent_id" => $seminar->id,
            ]);
        }


        $training = $this->createCategory([
            "name" => "training",
            "parent_id" => null,
        ]);

        $subSeminar = [
            "gedung",
            "catering",
            "transportasi",
        ];

        foreach ($subSeminar as $name) {
            $this->createCategory([
                "name" => $name,
                "parent_id" => $training->id,
            ]);
        }


        // $this->makeVendorToken();
        
        // $allAds = $this->createManyAds($slotLimit, [
        //     "category_id" => $category->id, 
        //     "sub_category_id" => $subCategory->id
        // ]);
        
        // $this->addAdsToQueue($allAds[0], [1, 4]);
        // $this->addAdsToQueue($allAds[1], [2, 6]);
        // $this->addAdsToQueue($allAds[2], [3, 7]);

        // $guzzle = $this->prepareGuzzle();
        // $response = $guzzle->request('GET', 'topads/queue', [
        //     "query" => [
        //         "start_time" => 1,
        //         "end_time" => 7,
        //     ],
        //     // "http_errors" => false
        // ]);
        // // $this->printJSONResponse($response);
    }
}
