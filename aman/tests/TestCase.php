<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    protected $baseUrl = "http://localhost/buatacara";
    use CreatesApplication;
    use TestTrait;
}
