<?php
namespace Tests;

use GuzzleHttp;

trait TestTrait
{
  protected $adminToken = null;
  protected $vendorToken = null;

  protected function prepareGuzzle() {
    $guzzle = new GuzzleHttp\Client([
      'base_uri' => 'http://localhost/buatacara/api/',
    ]);
    return $guzzle;
  }

  protected function printPrettyJSONString($jsonString) {
      print json_encode(json_decode($jsonString), JSON_PRETTY_PRINT);
  }

  protected function printPrettyJSON($json) {
      print json_encode($json, JSON_PRETTY_PRINT);
  }

  protected function printJSONResponse($response) {
    print "\n";
    print "code: " . $response->getStatusCode();
    print "\n";
    $this->printPrettyJSONString($response->getBody());
    print "\n";
    print "==================================";
    print "\n";
  }

  protected function createVendor()
  {
      $payload = [
          "username" => "vendor",
          "email" => "vendor@gmail.com",
          "full_name" => "Vendor Role User",
          "phone" => "08743545345",
          "address" => "Perumahan",
          "password" => "vendor"
      ];

      $guzzle = $this->prepareGuzzle();
      $response = $guzzle->request('POST', 'users', [
          "json" => $payload
      ]);

      // $this->printJSONResponse($response);
      $data = json_decode($response->getBody()); 
      return $data;
  }

  protected function createAdmin()
  {
      $payload = [
          "username" => "admin",
          "email" => "admin@gmail.com",
          "full_name" => "Vendor Role User",
          "phone" => "08743545345",
          "address" => "Perumahan",
          "password" => "admin"
      ];

      $guzzle = $this->prepareGuzzle();
      $response = $guzzle->request('POST', 'users?createAdmin=true', [
          "json" => $payload
      ]);

      // $this->printJSONResponse($response);
      $data = json_decode($response->getBody()); 
      return $data;
  }

  protected function login($username, $password) 
  {
      $payload = [
          "username" => $username,
          "password" => $password,
      ];

      $guzzle = $this->prepareGuzzle();
      $response = $guzzle->request('POST', 'login', [
          "json" => $payload
      ]);

      // $this->printJSONResponse($response);
      $data = json_decode($response->getBody()); 
      return $data->token;
  }

  protected function makeAdminToken()
  {
    $this->createAdmin();
    $adminToken = $this->login("admin", "admin");
    $this->adminToken = $adminToken;
    return $this->adminToken;
  }

  protected function makeVendorToken()
  {
    $this->createVendor();
    $vendorToken = $this->login("vendor", "vendor");
    $this->vendorToken = $vendorToken;
    return $this->vendorToken;
  }

  protected function createCategory($payload) {
    $reqPayload = [
        "name" => $payload["name"],
        "parent_id" => $payload["parent_id"],
    ];

    $guzzle = $this->prepareGuzzle();
    $response = $guzzle->request('POST', 'categories', [
        "headers" => [
            "token" => $this->adminToken,
        ],
        "json" => $reqPayload
    ]);

    // $this->printJSONResponse($response);
    $data = json_decode($response->getBody()); 
    return $data;
}

}