<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJawabansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jawabans', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('pendaftar_id');

            $table->string('question_id');
            $table->string('question');
            $table->boolean('required');
            $table->string('type');
            $table->longText('value');
            $table->longText('answers')->nullable();

            $table->timestamps();

            $table->primary('id');
            $table->foreign('pendaftar_id')->references('id')->on('pendaftars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jawabans');
    }
}
