<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendaftarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftars', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('beasiswa_id');
            $table->string('nama');
            $table->string('email');
            $table->enum('gender', ['laki-laki', 'perempuan'])->nullable();
            $table->enum('status', ['diterima', 'ditolak', 'selesai'])->nullable();
            $table->enum('last_status', ['diterima', 'ditolak', 'selesai'])->nullable();
            $table->timestamps();

            $table->primary('id');
            $table->foreign('beasiswa_id')->references('id')->on('beasiswas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pendaftars', function (Blueprint $table) {
            //
        });
    }
}
