-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `auth`;
CREATE TABLE `auth` (
  `token` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`token`),
  KEY `auth_user_id_foreign` (`user_id`),
  CONSTRAINT `auth_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `auth` (`token`, `user_id`, `created_at`, `updated_at`) VALUES
('b0e74bf0-e794-11e8-8bb6-4b510a9fded5',	'68070bd0-dbb9-11e8-ad04-634e3bdce841',	'2018-11-13 22:37:24',	'2018-11-13 22:37:24');

DROP TABLE IF EXISTS `beasiswas`;
CREATE TABLE `beasiswas` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published_date` date DEFAULT NULL,
  `email_body` longtext COLLATE utf8mb4_unicode_ci,
  `konten` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `form` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `beasiswas` (`id`, `judul`, `published_date`, `email_body`, `konten`, `form`, `created_at`, `updated_at`) VALUES
('15283bc0-dcba-11e8-b58b-4f593ca859fb',	'Beasiswa Bank Indonesia',	'2018-10-31',	NULL,	'<ol>\n<li>Mahasiswa Jurusan/Prodi Muamalat (Ekonomi Islam)  dan  Perbankan (Ekonomi Syari’ah);</li>\n<li>Sekurang-kurangnya telah menyelesaikan 4 (empat) semester dan/atau telah menempuh 60 (enam puluh) SKS;</li>\n<li>Usia tidak lebih dari 23 (dua puluh tiga) tahun saat menerimam beasiswa;</li>\n<li>Mempunyai aktifitas sosial yang memiliki dampat manfaat bagi masyarakat dan lingkungan;</li>\n<li>Diutamakan berasal dari keluarga dengan latar belakang ekonomi kurang mampu;</li>\n<li>Tidak sedang menerima beasiswa, bekerja dan atau berada dalam status ikatan dinas dari lembaga/instansi lain;</li>\n<li>Penerima beasiswa diwajibkan aktif untuk mengikuti dan aktif berpartisipasi pada semua kegiatan yang diberikan dan diselenggarakan oleh Bank Indonesia</li>\n</ol>\n<p>Bank Indonesia akan melakukan seleksi wawancara guna menentukan kelayakan calon penerima beasiswa sebanyak 40 mahasiswa untuk 2 (dua) jurusan/prodi tersebut di atas.</p>',	'[{\"id\":\"fb3289bc-a4ac-4e82-9123-85226d8ac73c\",\"type\":\"checkbox\",\"question\":\"Nama\",\"answers\":[{\"text\":\"coba\",\"checked\":false},{\"text\":\"oke\",\"checked\":false},{\"text\":\"\",\"checked\":false}],\"required\":true},{\"id\":\"5d3f91e3-a900-4823-9b77-a5b16a42a994\",\"type\":\"text\",\"question\":\"Email\",\"answers\":[],\"required\":true},{\"id\":\"87c7c0b7-5e32-4308-92c6-e8327aac9897\",\"type\":\"text\",\"question\":\"NIM\",\"answers\":[],\"required\":true},{\"id\":\"5eca55c8-c278-4bed-bff0-691141038366\",\"type\":\"textarea\",\"question\":\"Alamat\",\"answers\":[],\"required\":false},{\"id\":\"802dbab1-7b60-4045-83ff-14bbd02f2bef\",\"type\":\"file\",\"question\":\"KHS\",\"answers\":[],\"required\":true},{\"id\":\"1cac02d3-84b7-4e8e-b677-061af7351402\",\"type\":\"file\",\"question\":\"KRS\",\"answers\":[],\"required\":true}]',	'2018-10-31 03:07:21',	'2018-11-14 03:45:45'),
('2b8cee30-0894-11e9-be29-65919455cd4b',	'Di local kok gak double yak',	NULL,	NULL,	'<p>aneh nih</p>',	NULL,	'2018-12-25 22:26:49',	'2018-12-25 22:26:49'),
('3426c3c0-dca2-11e8-bbe2-5da9407e5202',	'Create Lagi bung',	'2018-10-31',	NULL,	'<p>mantul lah</p>',	NULL,	'2018-10-31 00:16:25',	'2018-10-31 00:16:25'),
('3a0df9b0-dcbb-11e8-b9c2-ef438ecb7e1a',	'Beasiswa Supersemar',	NULL,	NULL,	'<ol>\n<li>Duduk pada Semester  IV, VI, VIII dan X Tahun Akademik 2013/2014</li>\n<li>Memiliki IP minimal 3,00</li>\n<li>Diutamakan dari keluarga kurang mampu, dinyatakan dengan Surat Keterangan Keluarga tidak mampu, dari Lurah/Desa setempat atau Surat Keterangan GAKIN rangkap 2 (dua)</li>\n<ol>\n<li>Foto copy KHS terakhir yang dilegalisir rangkap 2 (dua);</li>\n<li>Foto copy KTM rangkap 2 (dua);</li>\n<li>Foto copy KTP  rangkap 2 (dua);</li>\n<li>Surat Keterangan tidak sedang menerima beasiswa dari pihak manapun dibuat oleh Fakultas rangkap 2 (dua)</li>\n</ol>\n<li>Rajin dan berkelakuan Baik</li>\n</ol>',	'[{\"id\":\"e1999149-0085-48b1-9e20-a0035ac86439\",\"type\":\"file\",\"question\":\"KHS\",\"answers\":[],\"required\":false},{\"id\":\"57ed4c8c-0abf-48c9-ab0c-4da751115e5a\",\"type\":\"text\",\"question\":\"Mantul gak\",\"answers\":[],\"required\":false},{\"id\":\"0c8b0d0b-ec25-4aaa-b2ad-b5764f21d77c\",\"type\":\"checkbox\",\"question\":\"Checkbox\",\"answers\":[{\"text\":\"Mantul gak seh\",\"checked\":false},{\"text\":\"mantul dong\",\"checked\":false},{\"text\":\"mantul banget\",\"checked\":false}],\"required\":true},{\"id\":\"4c779ad3-be26-495f-bd56-efd7b1120648\",\"type\":\"file\",\"question\":\"KRS\",\"answers\":[],\"required\":false}]',	'2018-10-31 03:15:32',	'2018-12-20 01:12:43'),
('3f44de40-dca2-11e8-8d3f-8bdf5bedbd2b',	'Mantul gak neh?',	NULL,	NULL,	'<p>Mantul dooooong</p>\n<p>mantul banget</p>',	NULL,	'2018-10-31 00:16:43',	'2018-12-12 03:08:23'),
('622dd780-dcbd-11e8-9a12-6de22b606508',	'Beasiswa Baru',	'2018-10-31',	NULL,	'<p>ini isinya beasiswa baru</p>',	'[{\"id\":\"274ee77a-9a29-417f-bc01-fb1679ba0777\",\"type\":\"text\",\"question\":\"Nama\",\"answers\":[],\"required\":true},{\"id\":\"8cdbfab3-6c9d-49af-b210-5fd8f0cdec03\",\"type\":\"text\",\"question\":\"Email\",\"answers\":[],\"required\":true},{\"id\":\"37f69abc-d38c-4c4f-b022-4967563adccd\",\"type\":\"textarea\",\"question\":\"Alamat\",\"answers\":[],\"required\":false},{\"id\":\"3937c961-2fc3-4c57-9cd6-ac5e203e898b\",\"type\":\"file\",\"question\":\"KRS\",\"answers\":[],\"required\":false}]',	'2018-10-31 03:30:58',	'2018-10-31 03:30:58'),
('6c0c7b00-0892-11e9-994f-37038c921c35',	'Gak boleh double',	NULL,	NULL,	'<p>Mantul gak neh?</p>',	NULL,	'2018-12-25 22:14:18',	'2018-12-25 22:14:18'),
('6d9a7630-dbb9-11e8-92e0-e309ab7fe698',	'Judulnya',	NULL,	NULL,	'Kontennya',	'[{}]',	'2018-10-29 20:30:08',	'2018-11-22 23:08:24'),
('d110bfe0-e794-11e8-b31e-9bdef1a35160',	'Beasiswa Baru Banget',	'2018-11-14',	NULL,	'<p>ini isinya beasiswa baru</p>',	'[{\"id\":\"274ee77a-9a29-417f-bc01-fb1679ba0777\",\"type\":\"text\",\"question\":\"Nama\",\"answers\":[],\"required\":true},{\"id\":\"8cdbfab3-6c9d-49af-b210-5fd8f0cdec03\",\"type\":\"text\",\"question\":\"Email\",\"answers\":[],\"required\":true},{\"id\":\"37f69abc-d38c-4c4f-b022-4967563adccd\",\"type\":\"textarea\",\"question\":\"Alamat\",\"answers\":[],\"required\":false},{\"id\":\"3937c961-2fc3-4c57-9cd6-ac5e203e898b\",\"type\":\"file\",\"question\":\"KRS\",\"answers\":[],\"required\":false}]',	'2018-11-13 22:38:18',	'2018-11-13 22:38:18'),
('dde0a8e0-dca1-11e8-aa67-dfef3f4bfe66',	'Judul',	NULL,	NULL,	'<p>Mantul gan</p>',	NULL,	'2018-10-31 00:14:00',	'2018-11-22 23:08:39'),
('e796c090-0894-11e9-ae98-274af00b9e0d',	'Judulnya',	'2018-10-30',	NULL,	'Kontennya',	'[{}]',	'2018-12-25 22:32:04',	'2018-12-25 22:32:04'),
('ec2f9740-dcbb-11e8-bc49-3d7d97b5a71e',	'Beasiswa Mahasiswa Kurang Mampu',	NULL,	NULL,	'<h3>A. Persyaratan Akademik</h3>\n<p>Diperuntukan bagi mahasiswa yang sedang aktif menjalankan studi dan duduk pada semester II  dan maksimal semester X</p>\n<p>Memiliki <strong>rangking Indeks Prestasi Kumulatif (IPK) minimal 2,75.</strong></p>\n<h3>B. Persayaratan Administrasi</h3>\n<ol>\n<li>Surat Keterangan dari Fakultas bahwa mahasiswa calon penerima beasiswa tidak sedang menerima beasiswa dari pihak manapun dan masih aktif sebagai mahasiswa IAIN Sunan Ampel/tidak sedang dalam masa cuti studi (Surat Keterangan dibuat secara kolektif);</li>\n<li>Surat Keterangan tidak mampu dari Kepala Desa/Lurah atau pejabat yang berwenang  atau menyerahkan copy surat GAKIN yang masih berlaku;</li>\n<li>Foto copy Kartu Keluarga;</li>\n<li>Foto copy KHS yang telah dilegalisir;</li>\n<li>Foto copy KTM</li>\n<li>Foto copy Nomor Rekening Bank Tabungan Negara  (harus BTN cabang IAIN Sunan Ampel, bagi yang sudah memiliki rekening di BTN dan bukan BTN cabang IAIN Sunan Ampel agar diganti) dan bagi yang belum memiliki rekening bisa membuka rekening di Bank Rakyat Indonesia (BRI) Cabang Kaliasin Surabaya (Kemahasiswaan bisa memfasilitasi untuk membuka rekening tersebut);</li>\n<li>Rekening dinyatakan masih aktif dengan bukti legalisir dari Bank;</li>\n<li>Menandatangani Kesepakatan Bersama Bantuan Beasiswa Miskin, dengan membawa materai 6.000,- sebanyak 2 lembar.</li>\n<li>Penerima beasiswa tidak diperkenankan mengajukan cuti studi pada tahun berjalan;</li>\n<li>Kuota penerima beasiswa Fakultas Adab sebanyak    311    mahasiswa;</li>\n<li>Point 1 s/d 7 dibuat masing-masing rangkap 1 (satu);</li>\n</ol>\n<p><strong>Diprioritaskan bagi mahasiswa yg belum pernah mendapatkan beasiswa.</strong></p>',	'[{\"id\":\"4d357719-a2dc-46a0-b223-0af546b705c0\",\"type\":\"text\",\"question\":\"Mantul kok\",\"answers\":[],\"required\":false}]',	'2018-10-31 03:20:31',	'2018-12-20 00:18:33');

DROP TABLE IF EXISTS `jawabans`;
CREATE TABLE `jawabans` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pendaftar_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_id` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `answers` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jawabans_pendaftar_id_foreign` (`pendaftar_id`),
  CONSTRAINT `jawabans_pendaftar_id_foreign` FOREIGN KEY (`pendaftar_id`) REFERENCES `pendaftars` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `jawabans` (`id`, `pendaftar_id`, `question_id`, `question`, `required`, `type`, `value`, `answers`, `created_at`, `updated_at`) VALUES
('0110f337-08b3-4b81-9f9b-b0d38c181f23',	'25ba1ae0-03f4-11e9-8f85-83f222f5c2be',	'57ed4c8c-0abf-48c9-ab0c-4da751115e5a',	'Mantul gak',	0,	'text',	'',	'[]',	'2018-12-20 01:11:15',	'2018-12-20 01:11:15'),
('087f6b21-132c-472e-b3ec-611305e13d93',	'25ba1ae0-03f4-11e9-8f85-83f222f5c2be',	'required-nama',	'Nama',	1,	'text',	'Maul',	'[]',	'2018-12-20 01:11:15',	'2018-12-20 01:11:15'),
('09e3a7c0-0ccc-4c01-9ea4-40f503396f3c',	'25ba1ae0-03f4-11e9-8f85-83f222f5c2be',	'required-email',	'Email',	1,	'text',	'lailatulmaulida98@gmail.com',	'[]',	'2018-12-20 01:11:15',	'2018-12-20 01:11:15'),
('0e1a96f1-3ab7-4410-b13d-2ddf76f6c5f0',	'1a3c7d70-03f4-11e9-ae0e-b3f767c4a8de',	'required-nama',	'Nama',	1,	'text',	'Namaku',	'[]',	'2018-12-20 01:10:55',	'2018-12-20 01:10:55'),
('18f0a487-a54e-47f9-bf18-5bc2d054f554',	'25ba1ae0-03f4-11e9-8f85-83f222f5c2be',	'required-gender',	'Jenis Kelamin',	1,	'radio',	'[\"perempuan\"]',	'[{\"text\":\"laki-laki\",\"checked\":false},{\"text\":\"perempuan\",\"checked\":true}]',	'2018-12-20 01:11:15',	'2018-12-20 01:11:15'),
('3966567c-8eaf-4ccd-9d64-77b69b6e4138',	'1a3c7d70-03f4-11e9-ae0e-b3f767c4a8de',	'4c779ad3-be26-495f-bd56-efd7b1120648',	'KRS',	0,	'file',	'',	'[]',	'2018-12-20 01:10:55',	'2018-12-20 01:10:55'),
('97ee950a-eb52-4cd7-a058-7bfc02bb3915',	'1a3c7d70-03f4-11e9-ae0e-b3f767c4a8de',	'57ed4c8c-0abf-48c9-ab0c-4da751115e5a',	'Mantul gak',	0,	'text',	'',	'[]',	'2018-12-20 01:10:55',	'2018-12-20 01:10:55'),
('9df43a4f-b6b1-4a4d-9a59-b6817cf122f8',	'25ba1ae0-03f4-11e9-8f85-83f222f5c2be',	'0c8b0d0b-ec25-4aaa-b2ad-b5764f21d77c',	'Checkbox',	1,	'checkbox',	'[\"Mantul gak seh\"]',	'[{\"text\":\"Mantul gak seh\",\"checked\":true},{\"text\":\"mantul dong\",\"checked\":false},{\"text\":\"mantul banget\",\"checked\":false}]',	'2018-12-20 01:11:15',	'2018-12-20 01:11:15'),
('d9149c7c-18b7-4346-8128-5e2bb6891792',	'1a3c7d70-03f4-11e9-ae0e-b3f767c4a8de',	'0c8b0d0b-ec25-4aaa-b2ad-b5764f21d77c',	'Checkbox',	1,	'checkbox',	'[\"Mantul gak seh\"]',	'[{\"text\":\"Mantul gak seh\",\"checked\":true},{\"text\":\"mantul dong\",\"checked\":false},{\"text\":\"mantul banget\",\"checked\":false}]',	'2018-12-20 01:10:55',	'2018-12-20 01:10:55'),
('dc7590e2-1a78-456e-a7b8-7e78bcb0fc04',	'25ba1ae0-03f4-11e9-8f85-83f222f5c2be',	'e1999149-0085-48b1-9e20-a0035ac86439',	'KHS',	0,	'file',	'',	'[]',	'2018-12-20 01:11:15',	'2018-12-20 01:11:15'),
('def63601-f8a8-49ae-9c93-dd6159d46c35',	'1a3c7d70-03f4-11e9-ae0e-b3f767c4a8de',	'required-gender',	'Jenis Kelamin',	1,	'radio',	'[\"laki-laki\"]',	'[{\"text\":\"laki-laki\",\"checked\":true},{\"text\":\"perempuan\",\"checked\":false}]',	'2018-12-20 01:10:55',	'2018-12-20 01:10:55'),
('e0f402bb-0aa3-4723-b734-1337679d96ee',	'1a3c7d70-03f4-11e9-ae0e-b3f767c4a8de',	'required-email',	'Email',	1,	'text',	'tkj1pal@gmail.com',	'[]',	'2018-12-20 01:10:55',	'2018-12-20 01:10:55'),
('effe9f72-8d45-4d7e-840e-f7f534abea1e',	'25ba1ae0-03f4-11e9-8f85-83f222f5c2be',	'4c779ad3-be26-495f-bd56-efd7b1120648',	'KRS',	0,	'file',	'',	'[]',	'2018-12-20 01:11:15',	'2018-12-20 01:11:15'),
('fbb81ec5-05e6-4ecc-825e-ba4b2539d479',	'1a3c7d70-03f4-11e9-ae0e-b3f767c4a8de',	'e1999149-0085-48b1-9e20-a0035ac86439',	'KHS',	0,	'file',	'',	'[]',	'2018-12-20 01:10:55',	'2018-12-20 01:10:55');

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2018_09_14_114848_create_users_table',	1),
(2,	'2018_09_14_114849_create_auth_table',	1),
(3,	'2018_09_14_114849_create_beasiswas_table',	1),
(4,	'2018_09_14_114850_create_pendaftars_table',	1),
(5,	'2018_11_22_235938_create_jawabans_table',	2);

DROP TABLE IF EXISTS `pendaftars`;
CREATE TABLE `pendaftars` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `beasiswa_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('laki-laki','perempuan') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('diterima','ditolak','selesai') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_status` enum('diterima','ditolak','selesai') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pendaftars_beasiswa_id_foreign` (`beasiswa_id`),
  CONSTRAINT `pendaftars_beasiswa_id_foreign` FOREIGN KEY (`beasiswa_id`) REFERENCES `beasiswas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `pendaftars` (`id`, `beasiswa_id`, `nama`, `email`, `gender`, `status`, `last_status`, `created_at`, `updated_at`) VALUES
('1a3c7d70-03f4-11e9-ae0e-b3f767c4a8de',	'3a0df9b0-dcbb-11e8-b9c2-ef438ecb7e1a',	'Namaku',	'tkj1pal@gmail.com',	'laki-laki',	'selesai',	'ditolak',	'2018-12-20 01:10:55',	'2018-12-20 01:12:24'),
('25ba1ae0-03f4-11e9-8f85-83f222f5c2be',	'3a0df9b0-dcbb-11e8-b9c2-ef438ecb7e1a',	'Maul',	'lailatulmaulida98@gmail.com',	'perempuan',	'selesai',	'diterima',	'2017-12-20 01:11:15',	'2018-12-20 01:12:22');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `email`, `nama`, `password`, `created_at`, `updated_at`) VALUES
('68070bd0-dbb9-11e8-ad04-634e3bdce841',	'tkj1pal@gmail.com',	'Muhammad Naufal Rabbani',	'$2y$10$AmzGk9g9iz4u1CuMcuedreh5E5IFSHxFUFI7k938IaXCmqY6JfwyG',	'2018-10-29 20:29:59',	'2018-10-29 20:29:59'),
('cd870120-e054-11e8-ab4e-0d689d815610',	'bosnaufalemail@gmail.com',	'Naufal Rabbani',	'$2y$10$UmGLEQQU.L68YYsks2I4s.w9Vdt2j/Sq0WcoImn4sXzdjjCH3FHje',	'2018-11-04 17:12:26',	'2018-11-04 17:23:48');

-- 2019-02-26 13:52:53
